import json
import datetime
import logging
import uuid
import time
import os
import yaml
import logging 
import logging.config
from pykafka import KafkaClient
from connexion import NoContent
import connexion

MAX_EVENTS = 10
FILE_NAME = 'event.json'
CURRENT_RETRIES = 0
MAX_TRIES = 10
TIME_IN_SECONDS = 5


if "TARGET_ENV" in os.environ and os.environ["TARGET_ENV"] == "test":
    print("In Test Environment")
    APP_CONF_FILE = "/config/app_conf.yaml"
    LOG_CONF_FILE = "/config/log_conf.yaml"
else:
    print("In Dev Environment")
    APP_CONF_FILE = "app_conf.yaml"
    LOG_CONF_FILE = "log_conf.yaml"

with open(APP_CONF_FILE, 'r') as f:
    app_config = yaml.safe_load(f.read())

with open(LOG_CONF_FILE, 'r') as f:
    log_config = yaml.safe_load(f.read())
    logging.config.dictConfig(log_config)

logger = logging.getLogger('basicLogger')
logger.info('hello-world')
logger.info("App Conf File: %s" % APP_CONF_FILE)
logger.info("Log Conf File: %s" % LOG_CONF_FILE)


"""FUNCTION"""
def  unique_id():
    uid = str(uuid.uuid4())
    return uid

while CURRENT_RETRIES < MAX_TRIES:
    try:
        HOST = str(app_config["events"]["hostname"]) +':' +str(app_config["events"]["port"])
        client = KafkaClient(hosts=HOST)
        topic = client.topics[str.encode(app_config["events"]["topic"])]
        producer = topic.get_sync_producer()
        break
    except Exception as e:
        logger.debug('connection refused')
        time.sleep(TIME_IN_SECONDS)
        CURRENT_RETRIES += 1


"""Function"""
def report_temperature_reading(body):
    uid = unique_id()
    body['trace_id'] = uid
    logger.info("Requested event temperature with a unique id of %s"%uid)
   #response = requests.post(app_config['temperature']['url'], json = body, headers = headers )
   # host = str(app_config["events"]["hostname"]) +':' +str(app_config["events"]["port"])
   # client = KafkaClient(hosts=host)
   # topic = client.topics[str.encode(app_config["events"]["topic"])]
   # producer = topic.get_sync_producer()
    msg = { "type": "temperature_reading",
    "datetime" :
    datetime.datetime.now().strftime(
    "%Y-%m-%dT%H:%M:%S"),
    "payload": body }
    msg_str = json.dumps(msg)
    producer.produce(msg_str.encode('utf-8'))
    logger.info("Returned event temperature response (Id: %s) with status code %s"%(body["trace_id"], '201'))
    return NoContent, 201


"""Function"""  
def report_co2_reading(body):
    uid = unique_id()
    body['trace_id'] = uid
    logger.info("Requested event co2 with a unique id of %s"%uid)
    #response = requests.post(app_config['co2']['url'], json = body, headers = headers )
   #  host = str(app_config["events"]["hostname"]) +':' +str(app_config["events"]["port"])
   #  logger.debug(host)
   #  client = KafkaClient(hosts=host)
   #  topic = client.topics[str.encode(app_config["events"]["topic"])]
   #  producer = topic.get_sync_producer()
    msg = { "type": "co2_reading",
   "datetime" :
   datetime.datetime.now().strftime(
   "%Y-%m-%dT%H:%M:%S"),
   "payload": body }
    msg_str = json.dumps(msg)
    producer.produce(msg_str.encode('utf-8'))
    logger.info("Returned event co2 reading response (Id: %s) with status code %s"%(body["trace_id"], '201'))
    return NoContent, 201


"""Function"""
def health():
    return { "message": "Running"}, 200
    
app = connexion.FlaskApp(__name__, specification_dir='')
app.add_api("openapi.yaml",
      strict_validation = True,
      validate_responses = True)
if __name__ == "__main__":
   app.run(port=8080)
